/* Home view
    
    This 'home.js' represents the index view that shows the user form

    - a new user form contains
        - user
        - password
        - email
        - first name
        - last name
*/
const home = {
    template: `
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-8">
                    <h3>Create user form</h3>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                        <label for="inputEmail" class="form-label">User</label>
                        <input type="text" class="form-control" id="inputEmail" placeholder="bruce21" v-model="user">
                    </div>
                    <div class="col-4">
                        <label for="inputPassword" class="form-label">Password</label>
                        <input type="password" class="form-control" id="inputPassword" v-model="password">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-8">
                        <label for="inputEmail" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="inputEmail" placeholder="name@example.com" required v-model="email">
                        <div class="alert alert-danger" role="alert" v-if="msg">
                            {{msg}}
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                        <label for="inputFirstName" class="form-label">First Name</label>
                        <input type="text" class="form-control" id="inputFirstName" placeholder="Bruce" v-model="firstName">
                    </div>
                    <div class="col-4">
                        <label for="inputLastName" class="form-label">Last Name</label>
                        <input type="text" class="form-control" id="inputLastName" placeholder="Wayne" v-model="lastName">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-8">
                        <div class="alert alert-danger" role="alert" v-if="msgForm">
                            {{msgForm}}
                        </div>
                        <button type="button"
                            class="btn btn-primary m-2 fload-start"
                            @click="addUser()">
                            Create user
                        </button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data(){
        return {
            user: "",
            password: "",
            email: "",
            firstName: "",
            lastName: "",
            msg: "",
            msgForm: ""
        }
    },
    methods: {
        addUser(){
            /*
                Takes the form input data, validate it and send for save in the db
            */
            // validates if all fields was filled
            if (!this.user && !this.password && !this.email && !this.firstName && !this.lastName){
                this.msgForm = "Please fill all form values";
                return;
            }
            // validates if the email is a valid address
            if (this.isValidEmail()){
                this.msg = "Please enter a valid email";
                return;
            }
            this.msg = "";
            this.msgForm = "";
            // creates the user object
            user = {
                user: this.user,
                password: this.password,
                email: this.email,
                first_name: this.firstName,
                last_name: this.lastName
            };
            // sends the user data for save
            axios.post("http://localhost:8000/create_user/", user)
            .then((response)=>{
                console.log(response.data);
            });
        },
        isValidEmail(){
            /*
                Test the email address with an email regular expresion
                return if the address is valid or not
            */
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email)) {
                return false;
            } else {
                return true;
            }
        },
    }
};