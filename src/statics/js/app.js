/*
    This 'app.js' defines the Vue app that is loaded in the django project.
    
    - Loads the home component to show at the beginning
*/
const routes = [
    {path:'/', component:home}
];

const router=new VueRouter({
    routes
});

const app = new Vue({router});
app.$mount('#app');