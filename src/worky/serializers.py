"""serializers

    This 'serializers' provides a way of serializing and deserializing the data into different formats
"""
from django.contrib.auth.models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('pk', 'username', 'password', 'email', 'first_name', 'last_name')

    def create(self, data):
        """
            Takes the user data and check if the user doesn't exists, 
            
            - if the user exists then return it 
            - if the user doesn't exists then create it
        """
        if User.objects.filter(username=data['user']).exists():
            return User.objects.filter(username=data['user'])
        user = User.objects.create(
            username=data['user'],
            email=data['email'],
            first_name=data['first_name'],
            last_name=data['last_name']
        )
        user.set_password(data['password'])
        user.save()
        return user

    def find(self, id):
        """
            Receives id and search the user
            - if the user exists then return the user
            - if the user doesn't exists then return an empty list
        """
        if User.objects.filter(id=id).exists():
            return User.objects.filter(id=id)
        else:
            return []

    def get_all(self):
        """
            Returns all user saved in the system
        """
        return User.objects.all()