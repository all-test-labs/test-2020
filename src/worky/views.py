"""UserView

    This 'UserView' creates new users.

    - A new user is created only if doesn't exists using their username as unique field.
"""
from django.contrib.auth.models import User
from django.http import HttpResponse
from rest_framework import serializers
from django.views import View
import json

from worky.serializers import UserSerializer


class UserView(View):
    
    def post(self, request):
        """
            Takes data in the request and try to create a new user
            expected input:
            {
                "user": "",
                "password": "",
                "email": "",
                "first_name": "",
                "last_name": ""
            }
            returns a json representation of the user with their primary key
            expected output:
            {  
                "pk": "",
                "user": "",
                "email": "",
                "first_name": "",
                "last_name": ""
            }
        """
        data = json.loads(request.body)
        serialized = UserSerializer()
        # tries to create the user
        query_result = serialized.create(data)
        # validates if the user was created or fetched from the db
        if not isinstance(query_result, User):
            user = query_result.values()[0]
            # compose the expected output
            response = {
                "pk": user['id'],
                "user": user['username'],
                "password": user['password'],
                "email": user['email'],
                "first_name": user['first_name'],
                "last_name": user['last_name']
            }
        else:
            # compose the expected output
            response = {
                "pk": query_result.id,
                "user": query_result.username,
                "password": query_result.password,
                "email": query_result.email,
                "first_name": query_result.first_name,
                "last_name": query_result.last_name
            }
        return HttpResponse(json.dumps(response), content_type="application/json")

    def get(self, request, pk=None):
        """
            Could Receives the primary key for find an specific user
            
            - if the user exists then return the saved user without the password
            - if the user doesn't exists then return empty response with status code 204
            - if the primary key is not provided then return all users saved
        """
        serialized = UserSerializer()
        result = []
        if pk:
            query_result = serialized.find(pk)
        else:
            query_result = serialized.get_all()
        if query_result:
            for user in query_result:
                response = {
                    "pk": user.id,
                    "user": user.username,
                    "email": user.email,
                    "first_name": user.first_name,
                    "last_name": user.last_name
                }
                result.append(response)
            return HttpResponse(result, content_type="application/json")
        else:
            return HttpResponse({}, content_type="application/json", status=204)
            